package dto;

import lombok.Data;

@Data
public class TimeDto {

    private long timeStamp;
}
