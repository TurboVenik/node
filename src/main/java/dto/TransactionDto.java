package dto;

import enums.TransactionType;
import lombok.Data;

@Data
public class TransactionDto {

    private TransactionType type;
    private String publicKey;
    private String[] data;
    private long timeStamp;
}
