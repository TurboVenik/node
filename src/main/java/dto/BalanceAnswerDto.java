package dto;

import lombok.Data;

@Data
public class BalanceAnswerDto {

    private long sum;
}
