package dto;

import lombok.Data;

@Data
public class PostBlockDto {

    private String data;
}
