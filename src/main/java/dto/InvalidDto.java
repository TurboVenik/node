package dto;

import lombok.Data;

@Data
public class InvalidDto {

    private int ind;
    private String str;
}
