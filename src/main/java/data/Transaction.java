package data;

import enums.TransactionType;
import lombok.Data;

@Data
public class Transaction {
    /*
    * Length, Public Key SRC wallet,
    *E (Private key, (SRC wallet,
    *Arr [DST wallet, Sum],
    *optional for other transaction)
    * */

    private long id;
    private TransactionType type;
    private int length;
    private String publicKey;
    private long timeStamp;

    private TransactionData transactionData;
    private String[] data; // xyi

    public Transaction(long id, TransactionType type, String publicKey,
                       long timeStamp, TransactionData transactionData, String[] data) {
        this.id = id;
        this.type = type;
        this.publicKey = publicKey;
        this.timeStamp = timeStamp;
        this.transactionData = transactionData;
        this.data = data;
    }
}
