package data;

import lombok.Data;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Data
public class Block {

    List<Transaction> transactionList;
    private long id;
    private long timeStamp;
    private String previousHash;
    private int count;

    public Block(long id, long timeStamp, String previousHash) {
        this.id = id;
        this.timeStamp = timeStamp;
        this.previousHash = previousHash;
        this.count = 0;
        this.transactionList = new CopyOnWriteArrayList<Transaction>();
    }
}
