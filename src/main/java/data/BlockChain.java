package data;

import dto.InvalidDto;
import dto.PostBlockDto;
import dto.TimeDto;
import dto.TransactionDto;
import lombok.Data;
import utils.CriptUtils;
import utils.JsonMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import static enums.TransactionType.purchase;

@Data
public class BlockChain {

    private static volatile long transactionId = 0;
    final private List<Block> blockChain = new CopyOnWriteArrayList<Block>();

    CriptUtils criptUtils = new CriptUtils();
    JsonMapper jsonMapper = new JsonMapper();

    private Block tmpBlock;
    private int count;

    public void newBlock(String message, String myWallet, int emission) {

        String time = "";
//        try {
//            TimeDto timeDto = jsonMapper.fromJson(criptUtils.decrypt2(postBlockDto.getData(), serverKey), TimeDto.class);
//            timeStamp = timeDto.getTimeStamp();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        long timeStamp = 0;
        try {
            time = criptUtils.decrypt20(message);
            timeStamp = Long.parseLong(time);
            System.out.println("---------------------");
            System.out.println(timeStamp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tmpBlock == null) {
            tmpBlock = new Block(0, timeStamp, null);
        } else if (blockChain.size() == 0) {
            String hash = criptUtils.hash(jsonMapper.toJson(tmpBlock));
            tmpBlock.setPreviousHash(hash);

            blockChain.add(tmpBlock);
            count++;

            long id = tmpBlock.getId();

            tmpBlock = new Block(id + 1, timeStamp, hash);
        } else {
            blockChain.add(tmpBlock);
            count++;

            long id = tmpBlock.getId();
            String hash = criptUtils.hash(jsonMapper.toJson(tmpBlock));
            tmpBlock = new Block(id + 1, timeStamp, hash);
        }
        emissionTransaction(timeStamp, myWallet, emission);
    }

    public long findBalance(String SRCwallet) {
        long balance = 0;

        for (Block block : blockChain) {
            for (Transaction transaction : block.getTransactionList()) {
                if (Objects.equals(transaction.getTransactionData().getSRCwallet(), SRCwallet)) {
                    for (TransactionBody body : transaction.getTransactionData().getTransactionList()) {
                        balance-=body.getSum();
                    }
                }
            }
        }

        for (Block block : blockChain) {
            for (Transaction transaction : block.getTransactionList()) {
                for (TransactionBody transactionBody : transaction.getTransactionData().getTransactionList()) {
                    if (transactionBody.getDSTwallet().equals(SRCwallet)) {
                        balance += transactionBody.getSum();
                    }
                }
            }
        }

//        for (Transaction transaction : tmpBlock.getTransactionList()) {
//            for (TransactionBody transactionBody : transaction.getTransactionData().getTransactionList()) {
//                if (transactionBody.getDSTwallet().equals(SRCwallet)) {
//                    balance += transactionBody.getSum();
//                }
//            }
//        }
//
//        for (Transaction transaction : tmpBlock.getTransactionList()) {
//            if (Objects.equals(transaction.getTransactionData().getSRCwallet(), SRCwallet)) {
//                for (TransactionBody body : transaction.getTransactionData().getTransactionList()) {
//                    balance-=body.getSum();
//                }
//            }
//        }
        return balance;
    }

    private void emissionTransaction(long timeStamp, String myWallet, int emission) {

        TransactionData transactionData = new TransactionData();
        transactionData.setSRCwallet("0");

        TransactionBody transactionBody = new TransactionBody();
        transactionBody.setDSTwallet(myWallet);
        transactionBody.setSum(emission);

        transactionData.getTransactionList().
                add(transactionBody);

        Transaction transaction =
                new Transaction(++transactionId, purchase, myWallet,
                        timeStamp, transactionData, new String[0]);

        tmpBlock.getTransactionList().add(transaction);
    }

    public void newTransaction(TransactionDto transactionDto) {
        String json = "";
        try {
            System.out.println("data mas===========");
            System.out.println(Arrays.toString(transactionDto.getData()));
            System.out.println("data mas===========");
            System.out.println(transactionDto.getData().length);
            System.out.println("data len===========");

            List<InvalidDto> pizdaList = new ArrayList<>();

            for (String s : transactionDto.getData()) {
                InvalidDto pizda = new InvalidDto();
                String str = criptUtils.decrypt(s, transactionDto.getPublicKey()).trim();
                pizda.setStr(str.substring(1));
                pizda.setInd(Integer.parseInt(str.substring(0,1)));

                pizdaList.add(pizda);
            }

            Collections.sort(pizdaList, (o1,o2)->{
                if (o1.getInd() > o2.getInd()) {
                    return 1;
                } else if (o1.getInd() <o2.getInd()) {
                    return -1;
                } else {
                    return 0;
                }
            });


            for (InvalidDto pizda : pizdaList) {
                json += pizda.getStr();
            }

            System.out.println("decript ============");
            System.out.println(json);
            System.out.println("decript ============");

            TransactionData transactionData = jsonMapper.fromJson(json, TransactionData.class);

            String key = transactionDto.getPublicKey().replace("\n", "");
            key = key.replace(" ", "");
            key = key.replace("\t", "");
            System.out.println("+++++++++++++++++++");
            System.out.println(criptUtils.hash(transactionDto.getPublicKey()));

            if (!Objects.equals(criptUtils.hash(transactionDto.getPublicKey()), transactionData.getSRCwallet())) {
                System.out.println("ВОРЫ АЛЕРТ ПАМАГИТЕ");
                
                
                System.out.println(key);
                System.out.println("=====================");
                System.out.println(criptUtils.hash(key));
                System.out.println("=====================");
                System.out.println(transactionData.getSRCwallet());
                System.out.println("=====================");
                return;
            }

            long balance = findBalance(transactionData.getSRCwallet());
            long sum = 0;
            for (TransactionBody body : transactionData.getTransactionList()) {
                if (body.getDSTwallet().equals(transactionData.getSRCwallet())) {
                    return;
                }
                sum += body.getSum();
            }

            if (balance < sum) {
                return;
            }
            Transaction transaction =
                    new Transaction(++transactionId, transactionDto.getType(),
                            transactionDto.getPublicKey(), transactionDto.getTimeStamp(),
                            transactionData, transactionDto.getData());

            tmpBlock.getTransactionList().add(transaction);
            tmpBlock.setCount(tmpBlock.getCount() + 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
