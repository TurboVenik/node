package data;

import lombok.Data;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Data
public class TransactionData {

    private String SRCwallet; // хэш от кея паблик
    private List<TransactionBody> transactionList = new CopyOnWriteArrayList<TransactionBody>();
}
