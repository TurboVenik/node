package data;

import lombok.Data;

@Data
public class TransactionBody {

    private String DSTwallet; // хэш
    private long sum;
}
