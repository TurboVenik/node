package utils;

import com.amazonaws.auth.PEM;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.util.Random;

import javax.crypto.Cipher;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.PrivateKey;
import java.util.Base64;

@Component
public class CriptUtils {

    @Autowired
    JsonMapper jsonMapper;

    private Gson gson = new Gson();

    public String hash(String o) {
        return Hashing.sha256().hashString(o, StandardCharsets.UTF_8).toString();
    }

    public Object encript(Object o, Object key) {
        return o;
    }

    public String decrypt(String stringMessage, String privateKey) throws Exception {
        System.out.println("ТУТУТУТУТУТУТУТТУ");
        System.out.println(stringMessage);

        privateKey.replace(" ", "");

        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        InputStream in = new ByteArrayInputStream(privateKey.getBytes(StandardCharsets.UTF_8.name()));
        PrivateKey privKey = PEM.readPrivateKey(in);

        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("RSA/None/OAEPWithSHA512AndMGF1Padding", "BC");
        cipher.init(javax.crypto.Cipher.DECRYPT_MODE, privKey);

        byte[] decodedStr = Base64.getDecoder().decode(
                stringMessage.replace("\n", "").replace("\r", "").replace(" ", ""));
        byte[] plainText = cipher.doFinal(decodedStr);

        String s = new String(plainText, "UTF-8");
        System.out.println(s);
        return s;
    }

    public static String decrypt20(String msg) throws Exception {
        String privateKey = "-----BEGIN PRIVATE KEY-----\n" +
                "MIIBPQIBAAJBALqbHeRLCyOdykC5SDLqI49ArYGYG1mqaH9/GnWjGavZM02fos4l\n" +
                "c2w6tCchcUBNtJvGqKwhC5JEnx3RYoSX2ucCAwEAAQJBAKn6O+tFFDt4MtBsNcDz\n" +
                "GDsYDjQbCubNW+yvKbn4PJ0UZoEebwmvH1ouKaUuacJcsiQkKzTHleu4krYGUGO1\n" +
                "mEECIQD0dUhj71vb1rN1pmTOhQOGB9GN1mygcxaIFOWW8znLRwIhAMNqlfLijUs6\n" +
                "rY+h1pJa/3Fh1HTSOCCCCWA0NRFnMANhAiEAwddKGqxPO6goz26s2rHQlHQYr47K\n" +
                "vgPkZu2jDCo7trsCIQC/PSfRsnSkEqCX18GtKPCjfSH10WSsK5YRWAY3KcyLAQIh\n" +
                "AL70wdUu5jMm2ex5cZGkZLRB50yE6rBiHCd5W1WdTFoe\n" +
                "-----END PRIVATE KEY-----";

        System.out.println(msg);
        InputStream in = new ByteArrayInputStream(privateKey.getBytes(StandardCharsets.UTF_8.name()));
        PrivateKey privKey = PEM.readPrivateKey(in);
        System.out.println(msg);
        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("RSA/ECB/PKCS1Padding");

        cipher.init(Cipher.DECRYPT_MODE, privKey);
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(msg));
        System.out.println("decrypt--------------------------------");

        System.out.println( new String(plainText));
        return new String(plainText);
    }

    public String takePublicKey() {
        Random random = new Random();
        return String.valueOf(random.nextLong());
    }
}
