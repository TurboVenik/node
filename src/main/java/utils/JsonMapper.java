package utils;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;

@Component
public class JsonMapper {

    private Gson gson = new Gson();

    public String toJson(Object o) {
        return gson.toJson(o);
    }

    public <T> T fromJson(String json, Class<T> tClass) {
        return gson.fromJson(json, tClass);
    }
}
