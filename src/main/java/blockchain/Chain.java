package blockchain;

import data.BlockChain;
import dto.PostBlockDto;
import dto.TransactionDto;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import utils.CriptUtils;
import utils.JsonMapper;

import java.util.Objects;

@Component
@Data
public class Chain {

    @Autowired
    CriptUtils criptUtils;

//    private String myPrivate = "-----BEGIN PRIVATE KEY-----\n" +
//            "\n" +
//            "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDEVQYvDeYfBR+h\n" +
//            "\n" +
//            "TDsYwqusZ00oFSUPwOStyEsvjHS2i0eOr2mCVme0+AejjAI6PaKyZHMP+lJTnNoS\n" +
//            "\n" +
//            "sMRWkPeCL/Bi1rgez9XGJ21p9CBVivWaFRcCLc3hSe3eASF7zNw9QXLFXan1fKm/\n" +
//            "\n" +
//            "PqvT8gDg2WZztwXCz+SGvjVJdnxR50bPjFHiyHpICbuJWZtmfcXtM8Z2gMg7IrNj\n" +
//            "\n" +
//            "e3OETGtI3alYBFfPMiRGPSDIbaKsRtuEmQP83/pdtSnz9MNxE2jvvfPePRN8ABNv\n" +
//            "\n" +
//            "z93b4hlCnms1kE8OvPOGyfCnmWxf3fuaQsuXZlWLV0snAnXohYSWPziCpY/C/Dfz\n" +
//            "\n" +
//            "Dmqkr+3lAgMBAAECggEACa2yVhf+j+lp/4LZR/VyzpcyFceDEfV9oHaHBFNNfqE7\n" +
//            "\n" +
//            "mBRcJG17g8+VWtHgWzwmHNq4VGGP9Oi+1FyL9UOEvBF+bXTt0zEGNhRD/8zl2im1\n" +
//            "\n" +
//            "RVEEVxpoc5J4LDFMlASLl+Z10LZ3Fi+80310rYhY1DzLYOl1Gn8yohTW3iX953Cj\n" +
//            "\n" +
//            "5fctpoWoAmPXA5OZv3BW6G+E2ieUMkBG9JqMlXf5K2Wzlho/yKVe2sdZTXMG7wga\n" +
//            "\n" +
//            "BrgOETCFAFtPwvRTyX5QiYXzLz7SwROUGxj4aG06R0n1mKCu6mTr8FYh3NJhXA7T\n" +
//            "\n" +
//            "Insyfav7l86IBsn8e0wMKcVpl39DqzWaJsoGMJbI8QKBgQDvEQgSN/KCZ/z+dv3m\n" +
//            "\n" +
//            "P94tbwIhQEfuBSMc7CQhuvU4nAyZyqT0sDaYiXhOQ1ZqRmuspWmEOB/sfUxI8FZj\n" +
//            "\n" +
//            "ibw4zBDyabNfQqIC8YdqT6rcHgc3zKqclAdhxBHnY8LC39c8jzPw006RTD7XIue+\n" +
//            "\n" +
//            "QlyaAre4rgFm+afzCjiCON+lkQKBgQDSPRgZQqcEVPBHm26PfVfWZWXmc3WKh7wt\n" +
//            "\n" +
//            "0j9GiH/G5qvHwgADRT4bL93V4jq1aXBdazu4AjFeusQxKaVoYGi3lzn5dNqvk6c6\n" +
//            "\n" +
//            "D02pLjpkYm/yfJXwrOt2/0V2FT8u5zM+Rq211NWFoAOlfrSJoO2CpkNihP+CHDYH\n" +
//            "\n" +
//            "54i2HZFJFQKBgC5ZXPk7Q5co1hH3u+4B/+9LEFyl+8wXzuDyn2ayuoxJJOBEO4r9\n" +
//            "\n" +
//            "Bvi33k0+HOjp0kJcp4Js5zOQb1PM1NLjh0xzs+1rtM0JPCPZ8+kWnQekAr48ve8x\n" +
//            "\n" +
//            "e+TmgJod60qg3zfsg6NzNE5TO/O13rwyXYW2dSwnADqYuIkUODR3FlUhAoGAPLQt\n" +
//            "\n" +
//            "QCXxqZuI4XQswTu04A/ZE2hlGQHGQQjDAm+pTVoPYZdPf6I/o4P3rqkqwHTYX/D1\n" +
//            "\n" +
//            "hlUDwlF1EqjuvKuRsOj2cWPfPPEKSQZgKyhQ7elKX56/gIW9W1ery8Oowbc6IZIw\n" +
//            "\n" +
//            "2B7DJFkmlGN4Z4DS83VkYY77Y6sXosKkw8jXyV0CgYAuBHz+h5p74PU4Rt9JXdfN\n" +
//            "\n" +
//            "PYIPgOfaFpjricIS/C8wNei+IyOViNzoLRsW+GHP4NVAU9CAeECX6+XdnSbMvGOq\n" +
//            "\n" +
//            "rYIwJcuCwFbeK+4uhO4JmbJto4TUZA48E4wBfNyb3sNRbktB+cYwshMCXa3OWBmC\n" +
//            "\n" +
//            "MpnFLe2i48FHMNAQwrcqkA==\n" +
//            "\n" +
//            "-----END PRIVATE KEY-----\n";
//
//    private String myWallet = "-----BEGIN PRIVATE KEY-----\n" +
//            "\n" +
//            "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDEVQYvDeYfBR+h\n" +
//            "\n" +
//            "TDsYwqusZ00oFSUPwOStyEsvjHS2i0eOr2mCVme0+AejjAI6PaKyZHMP+lJTnNoS\n" +
//            "\n" +
//            "sMRWkPeCL/Bi1rgez9XGJ21p9CBVivWaFRcCLc3hSe3eASF7zNw9QXLFXan1fKm/\n" +
//            "\n" +
//            "PqvT8gDg2WZztwXCz+SGvjVJdnxR50bPjFHiyHpICbuJWZtmfcXtM8Z2gMg7IrNj\n" +
//            "\n" +
//            "e3OETGtI3alYBFfPMiRGPSDIbaKsRtuEmQP83/pdtSnz9MNxE2jvvfPePRN8ABNv\n" +
//            "\n" +
//            "z93b4hlCnms1kE8OvPOGyfCnmWxf3fuaQsuXZlWLV0snAnXohYSWPziCpY/C/Dfz\n" +
//            "\n" +
//            "Dmqkr+3lAgMBAAECggEACa2yVhf+j+lp/4LZR/VyzpcyFceDEfV9oHaHBFNNfqE7\n" +
//            "\n" +
//            "mBRcJG17g8+VWtHgWzwmHNq4VGGP9Oi+1FyL9UOEvBF+bXTt0zEGNhRD/8zl2im1\n" +
//            "\n" +
//            "RVEEVxpoc5J4LDFMlASLl+Z10LZ3Fi+80310rYhY1DzLYOl1Gn8yohTW3iX953Cj\n" +
//            "\n" +
//            "5fctpoWoAmPXA5OZv3BW6G+E2ieUMkBG9JqMlXf5K2Wzlho/yKVe2sdZTXMG7wga\n" +
//            "\n" +
//            "BrgOETCFAFtPwvRTyX5QiYXzLz7SwROUGxj4aG06R0n1mKCu6mTr8FYh3NJhXA7T\n" +
//            "\n" +
//            "Insyfav7l86IBsn8e0wMKcVpl39DqzWaJsoGMJbI8QKBgQDvEQgSN/KCZ/z+dv3m\n" +
//            "\n" +
//            "P94tbwIhQEfuBSMc7CQhuvU4nAyZyqT0sDaYiXhOQ1ZqRmuspWmEOB/sfUxI8FZj\n" +
//            "\n" +
//            "ibw4zBDyabNfQqIC8YdqT6rcHgc3zKqclAdhxBHnY8LC39c8jzPw006RTD7XIue+\n" +
//            "\n" +
//            "QlyaAre4rgFm+afzCjiCON+lkQKBgQDSPRgZQqcEVPBHm26PfVfWZWXmc3WKh7wt\n" +
//            "\n" +
//            "0j9GiH/G5qvHwgADRT4bL93V4jq1aXBdazu4AjFeusQxKaVoYGi3lzn5dNqvk6c6\n" +
//            "\n" +
//            "D02pLjpkYm/yfJXwrOt2/0V2FT8u5zM+Rq211NWFoAOlfrSJoO2CpkNihP+CHDYH\n" +
//            "\n" +
//            "54i2HZFJFQKBgC5ZXPk7Q5co1hH3u+4B/+9LEFyl+8wXzuDyn2ayuoxJJOBEO4r9\n" +
//            "\n" +
//            "Bvi33k0+HOjp0kJcp4Js5zOQb1PM1NLjh0xzs+1rtM0JPCPZ8+kWnQekAr48ve8x\n" +
//            "\n" +
//            "e+TmgJod60qg3zfsg6NzNE5TO/O13rwyXYW2dSwnADqYuIkUODR3FlUhAoGAPLQt\n" +
//            "\n" +
//            "QCXxqZuI4XQswTu04A/ZE2hlGQHGQQjDAm+pTVoPYZdPf6I/o4P3rqkqwHTYX/D1\n" +
//            "\n" +
//            "hlUDwlF1EqjuvKuRsOj2cWPfPPEKSQZgKyhQ7elKX56/gIW9W1ery8Oowbc6IZIw\n" +
//            "\n" +
//            "2B7DJFkmlGN4Z4DS83VkYY77Y6sXosKkw8jXyV0CgYAuBHz+h5p74PU4Rt9JXdfN\n" +
//            "\n" +
//            "PYIPgOfaFpjricIS/C8wNei+IyOViNzoLRsW+GHP4NVAU9CAeECX6+XdnSbMvGOq\n" +
//            "\n" +
//            "rYIwJcuCwFbeK+4uhO4JmbJto4TUZA48E4wBfNyb3sNRbktB+cYwshMCXa3OWBmC\n" +
//            "\n" +
//            "MpnFLe2i48FHMNAQwrcqkA==\n" +
//            "\n" +
//            "-----END PRIVATE KEY-----\n" +
//            "-----BEGIN PUBLIC KEY-----\n" +
//            "\n" +
//            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxFUGLw3mHwUfoUw7GMKr\n" +
//            "\n" +
//            "rGdNKBUlD8DkrchLL4x0totHjq9pglZntPgHo4wCOj2ismRzD/pSU5zaErDEVpD3\n" +
//            "\n" +
//            "gi/wYta4Hs/VxidtafQgVYr1mhUXAi3N4Unt3gEhe8zcPUFyxV2p9Xypvz6r0/IA\n" +
//            "\n" +
//            "4Nlmc7cFws/khr41SXZ8UedGz4xR4sh6SAm7iVmbZn3F7TPGdoDIOyKzY3tzhExr\n" +
//            "\n" +
//            "SN2pWARXzzIkRj0gyG2irEbbhJkD/N/6XbUp8/TDcRNo773z3j0TfAATb8/d2+IZ\n" +
//            "\n" +
//            "Qp5rNZBPDrzzhsnwp5lsX937mkLLl2ZVi1dLJwJ16IWElj84gqWPwvw38w5qpK/t\n" +
//            "\n" +
//            "5QIDAQAB\n" +
//            "\n" +
//            "-----END PUBLIC KEY-----";

    private String myHash = "714356767379c5871458a460b9ab5a375b5a32d3bcf7ca600ab94006a531581e";
    private int emission = 10;

    private BlockChain blockChain = new BlockChain();

    public void newBlock(String postBlockDto) {

        blockChain.newBlock(postBlockDto, myHash, emission);
        System.out.println(myHash);
    }

    public long findBalance(String SRCwallet) {
        return blockChain.findBalance(SRCwallet);
    }

    public void newTransaction(TransactionDto transactionDto) {
        blockChain.newTransaction(transactionDto);
    }
}
