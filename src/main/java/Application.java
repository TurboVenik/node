import config.ApplicationConfiguration;
import org.springframework.boot.SpringApplication;

import java.security.Security;

public class Application {

    public static void main(String[] args) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        SpringApplication.run(ApplicationConfiguration.class, args);
    }
}
