package controllers;

import blockchain.Chain;
import dto.BalanceAnswerDto;
import dto.PostBlockDto;
import dto.TransactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import utils.CriptUtils;
import utils.JsonMapper;

@RestController
public class BlockChainController {

    @Autowired
    CriptUtils criptUtils;

    @Autowired
    JsonMapper jsonMapper;

    @Autowired
    Chain chain;

    /**
     * @param dto создания блока в формате:
     *            json {
     *            long timeStamp;
     *            }
     *            example{
     *            timeStamp: 1231; // long
     *            }
     * @summary Принять tmp Block/(синхронизация)
     */
    @RequestMapping(value = "/block", method = RequestMethod.POST)
    public void postBlock(@RequestBody String dto) {
        System.out.println(dto);

        //PostBlockDto postBlockDto = jsonMapper.fromJson(dto, PostBlockDto.class);
        chain.newBlock(dto);

        //System.out.println(chain.getBlockChain());
    }

    /**
     * @param dto создания транзакции в формате:
     *            json {
     *            private TransactionType type;
     *            private String publicKey;
     *            private String data;
     *            long timeStamp;
     *            }
     *            example {
     *            type: "purchase",
     *            publicKey: 12331212331212213;
     *            data: [123,213,12,31,24,41];
     *            timeStamp: 12312312312
     *            }
     * @summary обавить транзакцию, если она валидна
     */
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public void postTransaction(@RequestBody String dto) {
        TransactionDto transactionDto = jsonMapper.fromJson(dto, TransactionDto.class);
        System.out.println(transactionDto);
        chain.newTransaction(transactionDto);
    }

    /**
     * @return answerDto BalanceAnswerDto в формате:
     *          json {
     *          long sum;
     *          }
     *          example {
     *          sum: 1231; // long
     *          }
     * @summary возврашает баланс кошелька с хэшем SRCwallet
     */
    @RequestMapping(value = "/balance/{SRCwallet}", method = RequestMethod.GET)
    public BalanceAnswerDto getBalans(@PathVariable String SRCwallet) {
        long answer = chain.findBalance(SRCwallet);
        BalanceAnswerDto answerDto = new BalanceAnswerDto();

        answerDto.setSum(answer);
        return answerDto;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void test() {
        try {
            criptUtils.decrypt("","");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
